<?php
$serverName = "localhost";
$username="root";
$password="";
$database="web_learn";
try{
    $connection = new PDO("mysql:host=$serverName;dbname=$database", $username, $password);
    $connection -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e){
    echo "Connection failed". $e->getMessage();
}
?> 

