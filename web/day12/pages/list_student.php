<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Student</title>
</head>

<body>

    <?php 
        include ("../variable.php");

        // Get data student từ database 
        include ('../connection.php');
        $getData = "SELECT * FROM `student`";
        $getQuantity = "SELECT COUNT(`student`.id) AS COUNT FROM `student`";
        $count = $connection -> query($getQuantity);
        $datas = $connection -> query($getData);    

        if($_SERVER["REQUEST_METHOD"] == "POST"){
            $_COOKIE["department"] = $_POST["department"];
            $_COOKIE["keyword"] = $_POST["keyword"];
        }
    ?>

    <div class="center">
        <div class="wrapper search">
            <div class="flex justify-center">
                <div>
                    <div class="label-row department">
                        <span class="compulsory">Khoa</span>
                    </div>
                    <div class="label-row department">
                        <span>Từ khóa</span>
                    </div>
                </div>
                <div>
                <form action="" method="POST">
                    <div class="label-row department">
                        <div class="select-box">
                            <input id="select-input" readonly type="text" name="department" value="<?php if(isset($_COOKIE["department"])){ echo $_COOKIE["department"];} ?>">
                            <div class="arrow-down" id="button-dropdown"></div>
                            <ul class="dropdown hide" tabindex="-1">
                                <?php
                                    foreach ($departments as $department => $department_value) {
                                        echo "<li value=$department>$department_value</li>";                        
                                    }
                                ?>  
                            </ul>
                        </div>
                    </div>
                    <div class="label-row keyword">
                        <input type="text" name="keyword" value="<?php if(isset($_COOKIE["keyword"])){ echo $_COOKIE["keyword"];} ?>">
                    </div>
                    <div class="submit">
                            <button name="delete" id="delete-button" class="btn">Xóa</button>
                            <input class="ml-10" type="submit" value="Tìm kiếm" name="submit">
                        </div>
                    </div>
                </form>
            </div>
            <div class="flex justify-between items-center mt-40">
                <div class="number-student">
                    <span>Số sinh viên tìm thấy:</span>
                    <span>
                        <?php while($quantity = $count -> fetch(PDO::FETCH_ASSOC)) {
                            echo $quantity["COUNT"];
                        }?>
                    </span>
                </div>
                <div class="add-button">
                    <form action="../index.php">
                        <input type="submit" class="button-md" value="Thêm">
                    </form>
                </div>
            </div>
            <div class="table-student mt-40">
                <table>
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Tên sinh viên</th>
                            <th>Khoa</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $order = 0; 
                            while ($data = $datas -> fetch(PDO::FETCH_ASSOC)) {
                                $order++;
                                echo '<tr>
                                    <td>'.$order.'</td>
                                    <td>'.$data['name'].'</td>
                                    <td>'.$departments[$data['faculty']].'</td>
                                    <td class="action-table">
                                        <input type="submit" class="button-sm" value="Xóa">
                                        <input type="submit" class="button-sm ml-10" value="Sửa">
                                    </td>
                                </tr>';
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
<style>
<?php 
    include '../styles/global.css';
    include '../styles/search.css';
?>
</style>
<script src="../js/dropdown.js"></script>
<script>
    document.addEventListener("DOMContentLoaded", () => {
       document.querySelector("#delete-button").addEventListener("click", (e) => {
            e.preventDefault()
            document.querySelector("input[name=department]").setAttribute("value", "")
            document.querySelector("input[name=keyword]").value = ""
       }) 
    })
</script>
</html>